#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

int brojPonavljanja(vector<int>& brojevi, int trazeniBroj) {
	if (brojevi.size() == 1) {
		if (brojevi[0] == trazeniBroj) {
			return 1;
		} else {
			return 0;
		}
	}

	int sredina = brojevi.size() / 2;
	vector<int> brojeviLijevo;
	vector<int> brojeviDesno;

	for (int i = 0; i < sredina; i++) {
		brojeviLijevo.push_back(brojevi[i]);
	}
	for (int i = 0; i < (brojevi.size()) - sredina; i++) {
		brojeviDesno.push_back(brojevi[sredina + i]);
	}
	return brojPonavljanja(brojeviLijevo, trazeniBroj) + brojPonavljanja(brojeviDesno, trazeniBroj);
}

int main() {
	vector<int> brojevi{ 2, 6, 4, 4, 7, 7, 5, 4, 2, 4, 5, 3, 3 };
	cout << brojPonavljanja(brojevi, 4) << endl;
}
